import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/pages/Home";
import Otaku from "./components/pages/Otaku";
import Geek from "./components/pages/Geek";
import SignUp from "./components/pages/SignUp";

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/otaku" component={Otaku} />
          <Route path="/geek" component={Geek} />
          <Route path="/Sign-up" component={SignUp} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
