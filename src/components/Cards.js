import React from "react";
import CardItem from "./CardItem";
import "./Cards.css";

function Cards() {
  return (
    <div className="cards">
      <h1>Mon des otaku et geek </h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem
              src="./images/japan.jpg"
              text="Voulez vous voyage ou pas dans un pays ou le soeil et le vent regine"
              label="Voyage"
              path="/otaku"
            />
            <CardItem
              src="./images/esport.jpg"
              text="Bienvenue dans le monde du esport "
              label="Compétition"
              path="/geek"
            />
          </ul>
          <ul className="cards__items">
            <CardItem
              src="./images/ninja gaiden.jpg"
              text="Prenez place a un ninja qui veut sauve son village et le monde"
              label="Game "
              path="/geek"
            />
            <CardItem
              src="./images/pgw.jpg"
              text="Salon des jeux video pour tous les fan de jeu "
              label="Salon"
              path="/geek"
            />
            <CardItem
              src="./images/hinka.jpg"
              text="Venez rejoindre hinta et kageyama dans le monde du volley"
              label="Anime"
              path="/otaku"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
