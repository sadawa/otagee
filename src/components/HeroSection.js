import React from "react";
import { Button } from "./Button";
import "./HeroSection.css";
import "../App.css";
import anime from "../video/anime.mp4";

function HeroSection() {
  return (
    <div className="hero-container">
      <video src={anime} autoPlay loop muted />
      <div className="hero-btns">
        <Button
          className="btns"
          buttonStyle="btn--outline"
          buttonSize="btn--large"
        >
          C'EST PARTI
        </Button>
        <Button
          className="btns"
          buttonStyle="btn--primary"
          buttonSize="btn--large"
        >
          VOIR LA VIDEO <i className="far fa-play-circle" />
        </Button>
      </div>
    </div>
  );
}

export default HeroSection;
