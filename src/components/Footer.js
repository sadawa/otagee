import React from "react";
import "./Footer.css";
import { Button } from "./Button";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <div className="footer-container">
      <section className="footer-subscription">
        <p className="footer-subscription-heading">
          Rejoins dans l'aventure et reçois tous les news
        </p>
        <p className="footer-subscription-text">
          Tu peux de désincire a tous moment
        </p>
        <div className="input-areas">
          <form>
            <input
              className="footer-input"
              name="email"
              type="email"
              placeholder="Ton adresse email"
            />
            <Button buttonStyle="btn--outline">GOGO t'inscrire</Button>
          </form>
        </div>
      </section>
      <div className="footer-links">
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>Nous</h2>
            <Link to="/sign-up">GO GO </Link>
            <Link to="/">expertises</Link>
            <Link to="/">politique-de-confidentialite</Link>
            <Link to="/">Rejoins nous </Link>
            <Link to="/">Mentions légales</Link>
          </div>
          <div className="footer-link-items">
            <h2>Contact</h2>
            <Link to="/">Contact</Link>
            <Link to="/">Support</Link>
            <Link to="/">Sponsor </Link>
            <Link to="/">Remarque</Link>
          </div>
        </div>{" "}
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>Video</h2>
            <Link to="/">Envoye une vidéo </Link>
            <Link to="/">Agence</Link>
            <Link to="/">Influencer</Link>
            <Link to="/">Ambassadeur </Link>
          </div>
          <div className="footer-link-items">
            <h2>Social</h2>
            <Link to="/sign-up">Instagram </Link>
            <Link to="/">Facebook</Link>
            <Link to="/">Youtube</Link>
            <Link to="/">Twitter </Link>
          </div>
        </div>
      </div>
      <section className="social-media">
        <div className="social-media-wrap">
          <div className="footer-logo">
            <Link to="/" className="social-logo">
              OTAGEE
              <i className="fab fa-typo3" />
            </Link>
          </div>
          <small className="website-rights">OTAGEE © 2020</small>
          <div className="social-icons">
            <Link
              className="social-icon-link facebook"
              to="/"
              target="_blank"
              aria-label="Facebook"
            >
              <i className="fab fa-facebook-f" />
            </Link>
            <Link
              className="social-icon-link instagram"
              to="/"
              target="_blank"
              aria-label="Instagram"
            >
              <i className="fab fa-instagram" />
            </Link>
            <Link
              className="social-icon-link youtube"
              to="/"
              target="_blank"
              aria-label="Youtube"
            >
              <i className="fab fa-youtube" />
            </Link>
            <Link
              className="social-icon-link twitter"
              to="/"
              target="_blank"
              aria-label="Twitter"
            >
              <i className="fab fa-twitter" />
            </Link>
            <Link
              className="social-icon-link twitter"
              to="/"
              target="_blank"
              aria-label="LinkedIn"
            >
              <i className="fab fa-linkedin" />
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footer;
